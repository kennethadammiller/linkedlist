#include <iostream>
#define NODE_COUNT 3
using namespace std;

struct node { 
  node * next;
  int i;
};

void say_hello() {
  cout << "hello world" << endl;
}

int main(void) {
  node arr[NODE_COUNT];
  for (unsigned int i = 0; i<NODE_COUNT ; i++) {
    arr[i].i=i*10;
    arr[i].next = (i+1>NODE_COUNT) ? NULL : &(arr[i+1]);
  }
  for (unsigned int i = 0; i<NODE_COUNT ; i++) {
    cout << arr[i].i << ", ";
  }
  cout << endl; cout.flush();
}
